This repository holds the data and machine learning models to help
with GitLab issue triaging.

### Training data

The data is manually generated from the Rails console.

See [`data/README.md`](data/README.md) for details.

### Training

The `.gitlab-ci.yml` automates the training of this model by:

1. Extracting the encrypted data into the `data/` directory.
1. Converting the Jupyter notebook in `notebooks/classify_groups.ipynb` to a Python script.
1. Running the script, which outputs the trained model and tokenizer state in the `models/` directory.

Training this model requires a GPU with at least 26 GB of RAM. The
following GitLab Runner config illustrates how you might spin up a
custom Google N1 machine to do this:

#### Docker Machine config

```toml
[[runners]]
  name = "test-ml-runner-manager"
  url = "https://gitlab.com/"
  token = "REDACTED"
  executor = "docker+machine"
  [runners.docker]
    tls_verify = false
    image = "ruby:2.7"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    gpus = "all"
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
  [runners.machine]
    IdleCount = 0
    IdleTime = 600
    MaxBuilds = 2
    MachineDriver = "google"
    MachineName = "stanhu-ci-ml-%s"
    MachineOptions = ["google-project=gitlab-internal-153318",
      "google-disk-size=50",
      "google-disk-type=pd-ssd",
      "google-machine-type=n1-custom-4-26624",
      "google-accelerator=count=1,type=nvidia-tesla-p4",
      "google-maintenance-policy=TERMINATE",
      "google-machine-image=https://www.googleapis.com/compute/v1/projects/deeplearning-platform-release/global/images/family/tf2-ent-2-3-cu110", 
      "google-metadata=install-nvidia-driver=True"
    ]
    OffPeakTimezone = ""
    OffPeakIdleCount = 0
    OffPeakIdleTime = 0
```

## Running a manual mop up

Occasionally things go wrong and tanuki-stan doesn't run, or fails to update issues.
For example, when the PrAT expires. 

After failure, there may be a gap- a number of issues which need to be processed
(the default scheduled job only processes issues created "yesterday").

If only "yesterday" needs re-running a maintainer can simply run [the scheduled pipeline](https://gitlab.com/gitlab-org/ml-ops/tanuki-stan/-/pipeline_schedules).

For non-maintainers, or if older issues need processing:

* Create a new branch and raise a merge request
* A pipeline will be created with a manual `classify` job
* Select the job and populate the variables:
  * `START_DATE`: `yyyy-mm-dd`
  * `PRODUCTION_MODE`: `1`
  * `PRIVATE_TOKEN`: `<tanuki-stan scheduled pipeline password>` from 1Password engineering vault
* Run the job and validate the log output
* Close the merge request and delete the branch

You should see the API call made to grab issues with the relevant date and checking a few of the issues listed you should see both a comment and the addition of the relevant labels.

## Excluding labels from use

The label classifier will attempt to use labels that it trained against in `models/label_tokenizer.json`.

However, labels often change. Instead of retraining the whole data set, you may want the label classifier
to ignore certain labels and attempt to find the next best prediction.

In `scripts/config.yml`, you can add labels that will be skipped. For example, if you want to
skip over `group::package`, you can add an entry to the YAML file:

```yaml
labels:
  deny:
    - group::some group
    - group::package
```

## References

* YouTube: [TanukiStan: Using Machine Learning for GitLab Issue Triaging](https://www.youtube.com/watch?v=Q4ShY5DsIBY)
* Google Slides Presentation: [Using ML for GitLab Issue Triaging](https://docs.google.com/presentation/d/16Y6GTOY_AffD8vEbE4jkbQw_0s1hC5RbRM-PbuKBAWk/edit?usp=sharing)
